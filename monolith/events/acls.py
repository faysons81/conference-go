from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Use the Pexels API
    url = f'https://api.pexels.com/v1/search?query={city}+{state}?per_page=1?page=1'
    headers = {
        'Authorization': PEXELS_API_KEY
    }
    response = requests.get(url, headers=headers)
    content = json.loads(response.content)
    photo_url = content["photos"][0]["url"]
    return photo_url


def get_weather_data(city, state):
    # Use the Open Weather API
    params = {
        'q':f"{city},{state},US",
        'limit': 1,
        'appid': OPEN_WEATHER_API_KEY
    }
    geocode_url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(geocode_url, params=params)
    content = json.loads(response.content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        'appid': OPEN_WEATHER_API_KEY,
    }

    weather_url = "http://api.openweathermap.org/data/2.5/weather"

    weather_response = requests.get(weather_url, params=params)
    weather_data = json.loads(weather_response.content)
    try:
        return {
                'temp': weather_data['main']['temp'],
                'description': weather_data['weather'][0]['description']
        }
    except(KeyError, IndexError):
        return None
